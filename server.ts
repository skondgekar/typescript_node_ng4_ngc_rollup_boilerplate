/**
 * Importing required functions
 */
import * as express from "express";

import { Application, Request, Response } from "express";

import { config } from "./objects/config";

let app = express();


/**
 * Router conigurations
 * 
 */
import { route } from "./routes/test/routes";
route(app);

app.use("/public",express.static("./public"));
app.get("/",(req : Request, res : Response)=>{
    res.sendFile("./public/index.html",{
        root : __dirname
    })
})

/**
 * Running server
 */
let server = app.listen(config.Port,()=>{
    let host = config.ServerName;
    let port = server.address().port;
    console.log("App listening at http://%s:%s", host, port);
})

/**
 * Use following commands to compile these typescript files to javascript file
 * 
 * For Linux
 * ./node_modules/.bin/tsc
 * 
 * For Windows
 * .\node_modules\.bin\tsc
 * 
 * then run 
 * node server.js
 * 
 * browse to http://localhost:3002
 * You will see "Test Working"
 * 
 * To run angular 4 application browse to public folder and run following commands
 * npm start
 * 
 */