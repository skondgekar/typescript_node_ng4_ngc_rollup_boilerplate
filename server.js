"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Importing required functions
 */
const express = require("express");
const config_1 = require("./objects/config");
let app = express();
/**
 * Router conigurations
 *
 */
const routes_1 = require("./routes/test/routes");
routes_1.route(app);
app.use("/public", express.static("./public"));
app.get("/", (req, res) => {
    res.sendFile("./public/index.html", {
        root: __dirname
    });
});
/**
 * Running server
 */
let server = app.listen(config_1.config.Port, () => {
    let host = config_1.config.ServerName;
    let port = server.address().port;
    console.log("App listening at http://%s:%s", host, port);
});
/**
 * Use following commands to compile these typescript files to javascript file
 *
 * For Linux
 * ./node_modules/.bin/tsc
 *
 * For Windows
 * .\node_modules\.bin\tsc
 *
 * then run
 * node server.js
 *
 * browse to http://localhost:3002
 * You will see "Test Working"
 *
 * To run angular 4 application browse to public folder and run following commands
 *
 *
 */ 
