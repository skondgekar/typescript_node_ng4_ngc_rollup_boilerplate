/**
 * All configuration constants will be placed here and will be called as and when required
 */
export let config = {
    ServerName : "localhost",
    Port : 3004
}